//et array af alle spørgsmål og svar
questionArray = [
    {question:"I hvilket kontinent menes honningbien at være opstået?", answerA:"Asien", answerB:"Europa", answerC:"Nordamerika", answerD:"Afrika", rightAnswer:"a", correct:"Korrekt! Honningbien hører til slægten Apis, som menes at være opstået i Asien for cirka 35 millioner år siden.", wrong:"er forkert. Honningbien hører til slægten Apis, som menes at være opstået i Asien for cirka 35 millioner år siden."},
    
    {question:"Hvor hurtigt kan en bi flyve?", answerA:"10 kilometer i timen", answerB:"20 kilometer i timen", answerC:"30 kilometer i timen", answerD:"40 kilometer i timen", rightAnswer:"d", correct:"Korrekt! Biens topfart er cirka 40 kilometer i timen. Når den har fundet gode blomster at hente nektar eller pollen i, flyver den oftest med en hastighed på 25-30 kilometer i timen.", wrong:"er forkert. Biens topfart er cirka 40 kilometer i timen. Når den har fundet gode blomster at hente nektar eller pollen i, flyver den oftest med en hastighed på 25-30 kilometer i timen."},
    
    {question:"Hvor mange døgn tager det et biæg at blive til en arbejderbi?", answerA:"10 døgn", answerB:"21 døgn", answerC:"28 døgn", answerD:"41 døgn", rightAnswer:"b", correct:"Korrekt! Udviklingstiden kan dog variere en smule og afhængig af temperatur og foderets kvalitet.", wrong:"er forkert. Det tager et biæg 21 døgn at udklægge. Udviklingstiden kan dog variere en smule og afhængig af temperatur og foderets kvalitet."},
    
    {question:"Hvor mange arter af bier findes der på verdensplan?", answerA:"Mellem 3000-5000", answerB:"Omkring 8000", answerC:"Mere end 20.000", answerD:"Mere end 70.000", rightAnswer:"c", correct:"Korrekt! Globalt findes der mere end 20.000 arter af vilde bier.", wrong:"er forkert. Globalt findes der mere end 20.000 arter af vilde bier."},
    
    {question:"Hvordan kommunikerer bier?", answerA:"Ved hjælp af lyd", answerB:"Ved at berøre andre biers følehorn", answerC:"Ved hjælp af dans", answerD:"Bier kommunikerer slet ikke", rightAnswer:"c", correct:"Korrekt! For at kommunikere at der er føde tæt på stadet, danser bien runddans, hvor bien gentagne gange går i cirkler og skifter retning.", wrong:"er forkert. For at kommunikere at der er føde tæt på stadet, danser bien runddans, hvor bien gentagne gange går i cirkler og skifter retning."},
    
    {question:"Hvor mange gange kan en bi stikke et menneske før den dør?", answerA:"1 gang", answerB:"5-10 gange", answerC:"40-60 gange", answerD:"Ingen grænse", rightAnswer:"a", correct:"Korrekt! Har bien stukket et menneske, vrister den sig fri med det resultat, at brod og giftsæk bliver revet ud af bagkroppen. En bi kan derfor kun stikke et menneske én gang, da den dør af det.", wrong:"er forkert. Har bien stukket et menneske, vrister den sig fri med det resultat, at brod og giftsæk bliver revet ud af bagkroppen. En bi kan derfor kun stikke et menneske én gang, da den dør af det."}
];

//variablen til at holde styr på hvilket sprøgsmål man er på
var questionNumber = -1; 

//følgende funktion erstatter det nuværende spørgsmål med et andet
function nextQuestion() {
    
    $(".questionSkabelon .answers, .questionSkabelon .nextQuestion").each(function() {
        $(this).slideUp("slow");
    });
    
    $(".proceed:last").hide();
    questionNumber++;
    
    //cloner en skabelon og indsætter den efter den sidste
    $(".questionSkabelon:first").clone(true).attr("class", "questionSkabelon").insertAfter(".questionSkabelon:last");
    
    //animation der lukker skabelon hurtigt og åbner den langsomt
    if ($(".questionSkabelon").length > 2) {
        $(".questionSkabelon:last .answers, .questionSkabelon:last .nextQuestion").slideUp(0);
        $(".questionSkabelon:last .answers, .questionSkabelon:last .nextQuestion").slideDown("slow");
    }
    
    //indsætter data i den ny-klonede skabelon
    $(".question:last").text("Spørgsmål " + (questionNumber+1) + ": " + questionArray[questionNumber].question);
    $(".qa:last").attr({ id:"qa"+questionNumber, name:"q"+questionNumber }).next().attr("for", "qa"+questionNumber).text(questionArray[questionNumber].answerA);
    $(".qb:last").attr({ id:"qb"+questionNumber, name:"q"+questionNumber }).next().attr("for", "qb"+questionNumber).text(questionArray[questionNumber].answerB);
    $(".qc:last").attr({ id:"qc"+questionNumber, name:"q"+questionNumber }).next().attr("for", "qc"+questionNumber).text(questionArray[questionNumber].answerC);
    $(".qd:last").attr({ id:"qd"+questionNumber, name:"q"+questionNumber }).next().attr("for", "qd"+questionNumber).text(questionArray[questionNumber].answerD);
} //slutter nextQuestion()

//funktion til at checke spørgsmål
function checkQuestion() {
    //tjekker om et svar er valgt og hvis ikke så stopper den resten af funktionen
    if ($(".questionSkabelon:last input[type=radio]:checked").val() == undefined) {
        return;
    }

    //disabler alle radiobuttons i sidste skabelon
    $(".questionSkabelon:last input[type=radio]").each(function(index) {
        this.disabled = true;
    });
    
    //tjekker om svaret er rigtigt eller forkert og indsætter tekst i begynder afhængigt
    if ($(".questionSkabelon:last input[type=radio]:checked").val() == questionArray[questionNumber].rightAnswer) {
        $(".nextQuestion:last").prepend(questionArray[questionNumber].correct).css({ "color": "darkgreen", "font-weight": "bold" });
    } else {
        $(".nextQuestion:last").prepend("\"" + $(".questionSkabelon:last input[type=radio]:checked").next().text() + "\" "  + questionArray[questionNumber].wrong).css({ "color": "red", "font-weight": "bold" });
    }

    //tjekker om alle spørgsmål er blevet besvaret og slutter quizen hvis de er
    if (questionNumber == questionArray.length-1) {
        $(".checkQuestion").hide();
        return;
    }
    
    //gemmer check button og viser fortsæt button
    $(".checkQuestion:last").hide();
    $(".proceed:last").show();
} //slutter checkQuestion()

//ready
$(document).ready(function() {
    
    //click funktion til at åbne og lukke spørgsmål
    $("summary").click(function() {    
        $(this).next().slideToggle("slow");
        $(this).next().next().slideToggle("slow");
    });
    
    //click funktion til at forhindre "details" tag normale open og close
    $("summary").click(function(e) {    
        e.preventDefault();
    });
    
});