$(document).ready(function () {

    // two Arrays are instatiated
    var beePic = new Array;
    var honeyBePic = new Array;

    // Push values int Array
    beePic.push("1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg", "6.jpg", "7.jpg", "8.jpg");
    honeyBePic.push("1.jpg", "2.jpg", "3.jpg", "4.jpg", "5.jpg");

    // Defining half of the path of both honeyBee and otherBees
    var path = '/css/img/BeePictures/Bee';
    var beePath = '/css/img/BeePictures/HoneyBee/Bee'

    // Number of "otherBees" pictures shown
    var pictureNum = 3;


    // jQuery variabel 
    var $beePic = $('<div></div>').attr({
        id: 'imageOfBee',
        class: 'selectableHoneyB col span_1_of_2'
    });

    var $pic = $('<div></div>').attr({
        id: 'imageOfBee2',
        class: 'selectableHoneyB col span_1_of_2'

    });
    $("#images").append($beePic);
    $("#images").append($pic);

    // Generates 3 random otherBees Pics
    function imgRandom(beePic) {
        console.log(beePic)
        var randomNumber = Math.floor(Math.random() * beePic.length);
        console.log(randomNumber);
        var usedBeePic = beePic[randomNumber];
        console.log(usedBeePic);
        beePic.splice(randomNumber, 1);
        return usedBeePic;
    }

    // Generate one HoneyBee
    function imgHoneyRandom(honeyBePic) {
        var randomNumberHoney = Math.floor(Math.random() * honeyBePic.length);
        var usedHoneyBeePic = honeyBePic[randomNumberHoney];
        beePic.splice(randomNumberHoney, 1);
        return usedHoneyBeePic;
    }

    function notHoneyBeeDivs() {
        if ($("#imageOfBee").children().length <= 1) {
            appendOrPrepend = Math.floor(Math.random() * 2);
            if (appendOrPrepend == 0) {
                $("#imageOfBee").append($notHoneyBee);
            } else if (appendOrPrepend == 1) {
                $("#imageOfBee").prepend($notHoneyBee);
            }
        } else if ($("#imageOfBee2").children().length <= 1) {
            appendOrPrepend = Math.floor(Math.random() * 2);
            if (appendOrPrepend == 0) {
                $("#imageOfBee2").append($notHoneyBee);
            } else if (appendOrPrepend == 1) {
                $("#imageOfBee2").prepend($notHoneyBee);
            }
        }
    }

    // Append 1 random honeyBeePic-------------------------------------
    var honeyBeeImage = new Image();
    honeyBeeImage.src = beePath + imgHoneyRandom(honeyBePic);


    var $honeyBee = $('<div></div>').attr({
        id: 'honeyBee'
    });

    // Random generate number between 1 and 0
    var appendOrPrepend = Math.floor(Math.random() * 2);
    // noget kode der random vælger beepic eller pic

    if (appendOrPrepend == 0) {
        $("#imageOfBee").append($honeyBee);
        $("#honeyBee").append(honeyBeeImage);
    } else if (appendOrPrepend == 1) {
        $("#imageOfBee2").append($honeyBee);
        $("#honeyBee").append(honeyBeeImage);
    }
    console.log("Det her er append eller prepend: " + appendOrPrepend);
    // Finish appending -----------------------------------------------


    // Appends 3 otherBee pics-----------------------------------------
    var beeArray = new Array;

    console.log(beeArray);
    for (i = 0; i < pictureNum; i++) {
        //        // jQuery variabel
        var $notHoneyBee = $('<div></div>').attr({
            class: 'selectablePics',
            id: 'num' + i
        });
        var image = new Image();
        image.src = path + imgRandom(beePic);

        beeArray.push(image);
        // funktion fra tidligere bliver kørt
        notHoneyBeeDivs();

    }

    $("#num" + 0).append(beeArray[0]);
    $("#num" + 1).append(beeArray[1]);
    $("#num" + 2).append(beeArray[2]);

    // Finish appending ------------------------------------------------

    var textbox = document.getElementById("text");
    var pageURL = document.location.origin;

    // click event for otherBees
    $(".selectablePics img").click(function () {
        showMenu();

        var $imgPath = $(this).attr("src");
        //console.log($(this).attr("src"));

        // differentierer textboks inputtet
        if ($imgPath == path + '1.jpg' || $imgPath == path + '2.jpg') {
            textbox.innerHTML = 'FORKERT! <br><br> Dette er en vægbi.';
            // + "<img src=\"" + $imgPath + "\">"
        } else if ($imgPath == path + '3.jpg' || $imgPath == path + '4.jpg') {
            textbox.innerHTML = 'FORKERT! <br><br> Dette er en hvepsebi.';
        } else if ($imgPath == path + '5.jpg' || $imgPath == path + '6.jpg') {
            textbox.innerHTML = 'FORKERT! <br><br> Dette er en svirreflue.';
        } else if ($imgPath == path + '7.jpg' || $imgPath == path + '8.jpg') {
            textbox.innerHTML = 'FORKERT! <br><br> Dette er en pragtbi.';
        }

        //console.log(pageURL + path + "1.jpg");
    });

    // click event for honeyBees
    $("#honeyBee img").click(function () {
        showMenu();
        textbox.innerHTML = 'RIGTIGT! <br> <br> Du fandt honningbien!';

    });

});

// Menu 
function showMenu() {
    if ($("#menu").is(':hidden')) {
        $("#menu").show();
    } else if ($("#menu").is(':visible')) {
        $("#menu").hide();
        // reload page
        location.reload();
    }
}
