function ajax_getArticle(dataStr) {
    $.ajax({
            url: '/getArticle',
            method: 'POST',
            data: {
                wantedArticle: dataStr
            }
        })
        .done(function (data) {
            var choosenArticle = data;
            console.log(choosenArticle);
            $('#pdf').attr('data', choosenArticle);
        });
}

function getArticle() {
    var article1bool = localStorage.getItem("article1bool");
    var article2bool = localStorage.getItem("article2bool");
    var article3bool = localStorage.getItem("article3bool");
    var article4bool = localStorage.getItem("article4bool");
    var article5bool = localStorage.getItem("article5bool");
    var article6bool = localStorage.getItem("article6bool");

    if (article1bool == "true") {
        ajax_getArticle("article1");
    } else if (article2bool == "true") {
        ajax_getArticle("article2");
    } else if (article3bool == "true") {
        ajax_getArticle("article3");
    } else if (article4bool == "true") {
        ajax_getArticle("article4");
    } else if (article5bool == "true") {
        ajax_getArticle("article5");
    } else if (article6bool == "true") {
        ajax_getArticle("article6");
    } else {
        location.href = location.origin;
    }
}
