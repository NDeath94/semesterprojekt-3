//Stylesheet variables
var stylesheet3;
var stylesheet4;

//Initialize page with correct styling of cursor
$(document).ready(function () {
    console.log(document.styleSheets);
    stylesheet3 = document.styleSheets[3];
    stylesheet4 = document.styleSheets[4];
    stylesheet3.disabled = false;
    stylesheet4.disabled = true;

});

//AFRAME opacity component
AFRAME.registerComponent('model-opacity', {
    schema: {
        default: 1.0
    },
    //Set opacity on when model has loaded.
    init: function () {
        this.el.addEventListener('model-loaded', this.update.bind(this));
    },
    //Update opacity on mouseover and mouseleave
    update: function () {
        var mesh = this.el.getObject3D('mesh');
        var data = this.data;
        if (!mesh) {
            return;
        }
        mesh.traverse(function (node) {
            if (node.isMesh) {
                node.material.opacity = data;
                node.material.transparent = data < 1.0;
                node.material.needsUpdate = true;
            }
        });
        var el = this.el; // Reference to the entity.
        el.addEventListener('mouseenter', function () {
            mesh.traverse(function (node) {
                if (node.isMesh) {
                    node.material.opacity = "0.3";
                    node.material.transparent = true;
                    node.material.needsUpdate = true;
                }
            });
        });
        el.addEventListener('mouseleave', function () {
            mesh.traverse(function (node) {
                if (node.isMesh) {
                    node.material.opacity = "0";
                    node.material.transparent = true;
                    node.material.needsUpdate = true;
                }
            });
        });
    }
});

//AFRAME infobox component
AFRAME.registerComponent('info-on-click', {
    //Schema with different info values
    schema: {
        info0: {
            default: '<h1>ØJNE:</h1> <img src="css/img/anatomy/eyes.jpg" alt="eyes" class="beeimagevertical"> Bien har to typer øjne, nemlig sammensatte øjne og pandeøjne. De to store, sammensatte øjne på siden af hovedet består hver af omkring 5.000 enkeltøjne. Hvert af disse cirka 10.000 enkeltøjne ser kun en lille prik, men når alle synsindtrykkene samles i hjernen, bliver det til ét billede. Hvis man sammenligner biens tre kaster, så består et enkelt sammensat øje af cirka 5.000 enkeltøjne hos arbejderbien, cirka 3.500 hos dronningen og op mod 10.000 hos dronen. At dronen har flest enkeltøjne, hænger sammen med, at den skal kunne lokalisere og følge efter en flyvende dronning, som den kan parre sig med. Dronningen vil blot være en meget lille, mørk plet pá himlen, som dronen skal være i stand til at se. I forhold til mennesket kan bien se længere ind i det ultraviolette område af lysspektret, men til gengæld kortere i det røde. Det betyder, at bien ikke kan se rødt, men til gengæld ultraviolet. Blomsters farve bruges af bierne til at lokalisere fødekilder. Samtidig har mange blomster visuelle signaler, som er med til at lede bierne til deres nektarier, hvor de kan suge nektar og samtidig bestøve blomsten. Mange af de visuelle ledetráde reflekterer ultraviolet lys, hvorfor vi mennesker ikke er i stand til at se dem. Biens øjne er ligeledes følsomme over for polariseret lys, det vil sige lys, der kun har én bestemt retning, hvilket gør dem i stand til at lokalisere solen selv i overskyet vejr. Bier bruger også synet, når de skal navigere rundt i omgivelserne og blandt andet orientere sig efter indlærte pejlemærker i landskabet som for eksempel skovbryn, veje, søbredder og lignende. Bien har øverst på hovedet tre pandeøjne, som ikke danner billeder men som er meget følsomme over for lys. Pandeøjnene bruger bien til at holde balancen, når den flyver, så den ikke flyver sidelæns eller med ryggen mod jorden. Pandeojnene bruges også til en vis grad i forbindelse med biens orientering i landskabet.'
        },
        info1: {
            default: '<h1>MUNDDELE:</h1> <img src="css/img/anatomy/mouth.jpg" alt="mouth" class="beeimagevertical"> Selve biens mund er en spalte, som sidder nederst på hovedet. Over munden sidder overlæben, som egentlig blot er en plade, og under denne sidder biens to kindbakker. Kindbakkerne bruges som gribetænger og kan blandt andet forarbejde voks, gøre rent eller bide hul i celler. Bien har en snabel, som egentlig består af underlæben og kæberne, og inde i snablen er selve tungen. Tungen er besat med små har, og når bien stikker den ned i nektar og vand, vil det sætte sig på de små hår. Når bien ikke bruger tungen, ligger den sammenfoldet bag ved hovedet. <h2>HOVEDETS INDRE:</h2> Hovedet er forbundet med forkroppen via en streng, hvori blandt andet spiserøret og hjernens nervetráde løber. Hovedet indeholder foruden hjernen nogle kirtler, som udskiller forskellige sekreter. Kindbakkekirtlerne sidder der, hvor kindbakkerne har deres udspring. Disse kirtler har fire forskellige funktioner alt efter biens alder: Et par dage før den færdigudviklede bi kryber ud af sin celle, producerer kirtlerne et sekret, som kan opløse det låg, der forsegler cellen. Når bien har nået den alder, hvor den er ammebi (bier der passer yngel), producerer kirtlerne et sekret, som indgår i det foder, den giver ynglen. Når bien ikke længere er ammebi, begynder kirtlerne at producere et tredje sekret, som bruges, når bien skal forarbejde voks, propolis og pollen. Det fjerde og sidste sekret, disse kirtler producerer, er et alarmstof, som kan advare de øvrige bier. Bien har to fodersaftkirtler, som har en fælles udmunding i biens mund. Fodersaftkirtlerne opnår deres maksimale størrelse og aktivitet i den tid, hvor bien er ammebi. Kirtlerne producerer et proteinrigt sekret, der bruges som foder for ynglen. Efter tiden som ammebi skrumper kirtlen ind, men de er stadig aktive og producerer nu et sekret, som anvendes i forarbejdningen af honning.'
        },
        info2: {
            default: '<h1>BIENS KROP:</h1> <img src="css/img/anatomy/body.jpg" alt="body" class="beeimagehorizontal"> <h2>FORKROPPEN:</h2>Forkroppen Celler brystet) er placeret mellem bagkroppen og hovedet. Her sidder vingerne og biens tre par ben. <h2>BAGKROPPEN:</h2> Bagkroppen udgør den største del af biens krop og huser hovedparten af organerne. Hos både arbejderbi og dronning smalner bagkroppen til, mens den er afrundet hos dronen. <h2>ÅNDEHULLER:</h2> Langs siderne af for- og bagkrop er der placeret en række åndehuller (spirakler). Disse leder ind til et forgrenet net af luftrør (trakeer) og luftsække, hvorfra ilten transporteres rundt i kroppen. Selve vejrtrækningen sker ved, at bagkroppen trækkes sammen og foldes ud. <h2>FORDØJELSESSYSTEMET:</h2> I bagkroppens indre findes blandt andet biens fordøjelsessystem. Når bien indtager nektar og honning, føres det fra munden over i spiserøret, som strækker sig gennem forkroppen for at havne i honningmaven, der sidder forrest i bagkroppen. Honningmaven er en blære, hvori bien kan opbevare og transportere nektar, når den er ude for at samle i blomsterne. Honningmaven er forsynet med en ventil, som sikrer, at indholdet ikke løber videre over i tarmsystemet. Ventilen er besat med små hår, som filtrerer pollenkorn og urenheder fra nektaren og samtidig sikrer, at der ikke løber tarmindhold ind i honningmaven. Når bien skal have noget at æde, åbner den ventilen, så indholdet fra honningmaven kan passere over i tarmsystemet, hvorfra bien kan optage fodens næring. Når føden har passeret tarmsystemet, ender det til sidst i endetarmen. For at undgå eventuel smittespredning besørger bien ikke inde i bistadet. Om vinteren, hvor bien ikke kan komme ud er den derfor nødt til at holde sig. Den er således i stand til at opbevare en hel vinters ekskrementer i endetarmen. Bliver bierne forstyrret om vinteren, er der dog risiko for, at de kommer til at tømme tarmene inde bistadet. Den forste forårsdag, hvor temperaturen er høj nok til at flyve ud, tager blerne ud pá en såkaldt renselsesflyvning, og her får de mulighed for at tømme tarmen. Bagkroppen indeholder også fedtlegemer, hvor bien oplagrer fedt og proteiner, som den kan tære på i perioder med smalkost, for eksempel i løbet af vinteren. <h2>HJERTET:</h2> Indvendigt langs rygsiden af bagkroppen løber et rør. Det er biens hjerte. Hjertet er forbundet med bikroppens eneste egentlige åre (aorta), som løber hele vejen op til hjernen. Da åren kun løber mellem hjerte og hjerne, sker resten af blodets (kaldet hæmolymfe) cirkulation uden for åren ved, at det løber mellem organerne. <h2>BAGKROPPENS KIRTLER:</h2> Bagkroppen er forsynet med et antal kirtler. På undersiden af bagkroppen sidder fire par vokskirtler, der afsondrer vokspartikler. Disse samles til små plader (voksskæl). Pladerne skubbes ud mellem leddene på bagkroppen, hvorefter bien kan forarbejde dem og bruge dem til at bygge med. Når bien producerer voksplader, siger man, at den sveder voks. I forbindelse med biens brod sidder et par kirtler, som producerer duftstoffer (alarmferomoner), der advarer de andre bier, når bien føler sig truet. Bliver man stukket, vil andre bier opfange duftstofferne og gå til angreb.'
        },
        info3: {
            default: '<h1>BRODDEN:</h1> <img src="css/img/anatomy/stinger.gif" alt="stinger" class="beeimagevertical"> For enden af bagkroppen sidder brodden. Brodden er et par millimeter lang og forsynet med modhager. Når bien stikker et andet insekt, kan brodden trækkes ud igen, men når den stikker et menneske, vil den på grund af hudens tykkelse blive siddende. Har bien stukket et menneske, vrister den sig fri med det resultat, at brod og giftsæk bliver revet ud af bagkroppen. En bi kan derfor kun stikke et menneske én gang, da den dør af det. Får brodden lov at sidde i huden, bliver den ved med at pum- pe gift, indtil den er tømt. Dronningen har også en brod, men den er krum og ikke i samme grad forsynet med modhager. Brodden bruger hun til at slå andre dronninger ihjel med. Droner er brodløse.'
        },
        info4: {
            default: '<h1>VINGER:</h1> <img src="css/img/anatomy/wings.jpg" alt="Wings" class="beeimagehorizontal"> Bien har to par vinger. Hvert vingepar består af en for- og en bagvinge. Forvingerne er større end bagvingerne. Bagvingen er forsynet med en række kroge, som griber fat i forvingen og holder de to vinger sammen under flyvning. Når bien er i hvile, er vingerne frakoblet hinanden og ligger fladt hen ad kroppen. Vingerne er klare og forsynet med et net af årer, som stiver dem af. Årerne indeholder luftrør og nerver, som har forbindelse med vingernes sansehår I forkroppens indre sidder de store flyvemuskler. Disse muskler har, ud over at gøre bien i stand til at flyve, også den funktion, at de kan generere varme: Ved at vibrere med flyvemusklerne skaber bien varme, som bruges i forbindelse med temperaturregulering af blandt andet yngellejet og i vinterklyngen.'
        },
        info5: {
            default: '<h1>BEN:</h1> <img src="css/img/anatomy/legs.jpg" alt="legs" class="beeimagehorizontal"> Biens tre par ben inddeles i for-, mellem- og bagben. Benene er behårede og består af seks led. Yderst på benet sidder tåleddet, der er forsynet med et par kløer og en hæfteskive. Kløerne bruger bien til at gribe fat med, når den færdes på pu overflader. Hæfteskiven bruges, når bien færdes på glatte overflader; her fungerer den som en sugekop. På hvert forben er der et indhak, som bien bruger til at fjerne pollen fra følehornene. Mellembenene har stive børster, som hjælper bien med at rense kroppen for pollen. Bagbenene er besat med lange, indadbuede hår, som tilsammen danner en kurv. Denne kurv kaldes en pollenkurv, fordi bien bruger den til at opbevare pollen i, når den er ude for at samle denne i blomsterne. Når pollenkurven er fyldt, siger man, at bien har bukser på. Ved at blande pollen med nektar er bien i stand til at pakke det i kurven. Da hverken dronning eller droner samler pollen, mangler de begge pollen kurven på deres bagben. Benene er foldet ind under kroppen, når bien ikke er lastet med nektar eller vand. Har bien derimod fyldt sin mave, vil benene hænge lodret ned. Det gør de for at udligne den forskydning af tyngdepunktet, som opstår, når bagkroppen bærer den tunge last.'
        },
        info6: {
            default: '<h1>FØLEHORN:</h1> <img src="css/img/anatomy/antennas.png" alt="antennas" class="beeimagehorizontal"> Følehornene bestár af 12 led, hvilket gør dem meget bevægelige. De har mange forskellige funktioner. Overfladen er dækket med et tæt lag følehár, som blandt andet opfanger vibrationer og fungerer som en form for "øre". Følehornene spiller en stor rolle inde i det mørke bistade, hvor bierne føler sig frem. Biens følehorn er i stand til at registrere dufte, og lugtesansen spiller en vigtig rolle i biens liv. Hver bifamilie har sin egen helt unikke lugt, og bier, som ikke bærer bifamiliens lugt, vil normalt blive jaget væk. Blomsternes duft sætter sig også på bien. Når den har besøgt en god trækkilde (en blomst, som nektar kan suges ud fra) og vender hjem for at rekruttere flere bier, dufter den af den pågældende blomst. Denne duf kombineret med de øvrige informationer som den hjemvendte bi meddeler, gør de andre bier i stand til at finde frem til den gode trækkilde. Følehornene har også sanseorganer, som bruges til at bestemme blandt andet luftfugtighed og temperatur i bistadet, hvorved bien får informationer om klimaet inde i bistadet og kan handle ud fra disse. Klimaet i et bistade er meget velreguleret, sådan at temperatur og luftfugtighed er optimal for bifamiliens udvikling.'
        }
    },
    //Change cursor style on mouseover
    init: function () {
        var el = this.el; // Reference to the entity.
        var data = this.data;
        var textbox = document.getElementById("text");
        stylesheet3 = document.styleSheets[3];
        stylesheet4 = document.styleSheets[4];
        el.addEventListener('mouseenter', function () {
            stylesheet3.disabled = true;
            stylesheet4.disabled = false;
        });
        el.addEventListener('mouseleave', function () {
            stylesheet3.disabled = false;
            stylesheet4.disabled = true;
        });
        //Open infobox and show infor on mouseclick
        el.addEventListener('click', function () {
            showMenu();
            if (el == document.querySelector('#bee_eyes_id')) {
                textbox.innerHTML = data.info0;
            } else if (el == document.querySelector('#bee_mouth_id')) {
                textbox.innerHTML = data.info1;
            } else if (el == document.querySelector('#bee_body_id')) {
                textbox.innerHTML = data.info2;
            } else if (el == document.querySelector('#bee_stinger_id')) {
                textbox.innerHTML = data.info3;
            } else if (el == document.querySelector('#bee_wings_id')) {
                textbox.innerHTML = data.info4;
            } else if (el == document.querySelector('#bee_legs_id')) {
                textbox.innerHTML = data.info5;
            } else if (el == document.querySelector('#bee_feelers_id')) {
                textbox.innerHTML = data.info6;
            }
        });
    }
});

//Show and hide menu
function showMenu() {
    if ($("#menu").is(':hidden')) {
        $("#menu").show();
    } else if ($("#menu").is(':visible')) {
        $("#menu").hide();
    }
}
