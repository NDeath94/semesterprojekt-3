var currentQuestionNumber = 0;
var currentAnswerNumber = 0;

$(document).ready(function () {
    $("#menu").hide();
    $("#nextBtn").hide().click(function () {
        currentQuestionNumber++;
        createQuestionElement(currentQuestionNumber);
        $(this).hide();
    });

    $('#startbtn').click(function () {
        startMethod();
    });

    $('#Nejbtn').click(function () {

        $("#menu").hide();
    });
    $('#Jabtn').click(checkAnswer());


});

//Array, containing quiz questions, answers and correct answers
var questions = [{
    question: "Hvor mange dronninger er der i et bo?",
    choices: ["A: En dronning", "B: Én om sommeren og én om vinteren", "C: Der kan være mange", "D: Der er altid to af gangen"],
    correctAnswer: "0"
  }, {
    question: "Hvad er dronningens formål i et bistad?",
    choices: ["At lægge æg", "At lægge æg og indsamle nektar", "At lægge æg og passe larverne", "At angribe og forsvare boet mod andre dronninger"],
    correctAnswer: "0"
  }, {
    question: "Hvor mange dage går der, fra ægget bliver lagt, før man har en fuldvoksen bi?",
    choices: ["Efter 7 dage", "Efter 14 dage", "Efter 21 dage", "Efter 28 dage"],
    correctAnswer: "2"
  }, {
    question: "Hvad er den gennemsnitlige levealder for en arbejdsbi?",
    choices: ["2 uger", "4 uger", "6 uger", "8 uger"],
    correctAnswer: "2"
  }, {
    question: "Hvordan kommunikerer bierne med hinanden?",
    choices: ["Ved hjælp af lyde", "Ved hjælp af dans", "Ved at blinke", "Ved berøring"],
    correctAnswer: "1"
  }];

//Shows the "Are you sure-div"
function showMenu(can) {
    return function () {
        $('#Jabtn').attr('name', can);
        if ($("#menu").is(':hidden')) {
            $("#menu").show();
        } else if ($("#menu").is(':visible')) {
            $("#menu").hide();
        }
    }
}

//Checks if the chosen answer is correct && if there are any more questions left
//If the answer is correct, then color green - otherwise red.
//Plays win or loose-sound
function checkAnswer() {
    return function () {
        var n = $('#Jabtn').attr('name');
        var cqn = currentQuestionNumber;

        if (n == questions[cqn].correctAnswer) {
            $("#menu").hide();
            $('#choice' + (cqn + 1) + (parseInt(n) + 1)).css("background-color", "green");
            $('#wrong').get(0).pause();
            $('#correct').get(0).play();
            if (cqn < (questions.length - 1)) {
                $('#nextBtn').show();
            } else {
                $('#nextBtn').hide();
            }

        } else {
            $("#menu").hide();
            $('#choice' + (cqn + 1) + (parseInt(n) + 1)).css("background-color", "red");
            $('#correct').get(0).pause();
            $('#wrong').get(0).play();
            if (cqn < (questions.length - 1)) {
                $('#nextBtn').show();
            } else {
                $('#nextBtn').hide();
            }
        }

        doNotClick(cqn);

    }
}

//Runs createQuestionElement() when user hit 'start'
var index = 0;
function startMethod() {

    $('#start').hide();
    createQuestionElement(index);

}

//Creates the question element
function createQuestionElement(i) {
    currentAnswerNumber = 0;
    var qElement = $('#questions');
    qElement.text("");
    var question = $('<div class="questionBox" id="questionBox"> ').append('Spørgsmål ' + (i + 1) + ': ' + questions[i].question);
    qElement.append(question);

    var choices = $('<div class="choices">');
    for (var j = 0; j < questions[i].choices.length; j++) {
        var singleChoice = $('<div/>').attr({
                class: "choice",
                id: "choice" + (i + 1) + (j + 1)
            }).text(questions[i].choices[j])
            .click(showMenu(currentAnswerNumber))
            .css({

                "border": "5px solid #f7fd81",
                "border-radius": "10px",
                "background-color": "black",
                "text-align": "center",
                "width": "40%",
                "position": "relative",
                "display": "inline-block",
                "margin-left": "3%",
                "margin-bottom": "5%",
                "align-content": "center",
                "height": "40px",
                "padding": "10px",
                "float" : "left"

            }).hover(function () {
                $(this).css("background-color", "#FFBF05");

            }, function () {
                $(this).css("background-color", "black");

            });
        choices.append(singleChoice);
        currentAnswerNumber++;
    }
   
    choices.insertAfter("#questionBox");


}



//Makes choices unclickable
function doNotClick(i) {

    for (var k = 0; k < 4; k++) {

        $('#choice' + (i + 1) + (k + 1)).unbind('click mouseenter mouseleave');

    }


}
