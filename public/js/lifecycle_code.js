//Containers   
var conDynamic; //All moving parts of the life cycle
var conStatic; //All static elements of the life cycle
//Content for dynamic generation of containers and textbox 
var conEgg = {
    name: "conEgg",
    text: "Æg",
    content: "<h1>Æg</h1><img src='js/lib/images/beeEgg.jpg' class='textBoxImage'><p class='header'>Facts om biens æg:</p><p>Ægget er mælkehvidt og cirka 1,7 millimeter langt og 0,4 millimeter bredt. Et æg vejer 0,13 milligram, så når dronningen lægger 2.000 æg i døgnet, svarer det til, at hun lægger sin egen vægt i æg. Dronningen sætter et enkelt æg på bunden af hver celle, sådan at det står lodret. Hen imod slutningen af æggestadiet lægger ægget sig ned. Æggeskallen opløses og det er nu en lille larve, som ligger på bunden af cellen.</p>",
    image: "js/lib/images/beeEgg.png"
};
var conLarva = {
    name: "conLarva",
    text: "Larve",
    content: "<h1>Larve</h1><img src='js/lib/images/beeLarva.jpg' class='textBoxImage'><p class='header'>Facts om biens larve:</p><p>Den lille larve, der klækkes af ægget, er cirka 1 millimeter lang og ligger krummet sammen. I løbet af de seks dage, som larvestadiet varer, øger larven sin vægt op mod 1.000 gange. Larven er blind, næsten helt inaktiv og mangler ben - til gengæld har den mund og fordøjelseskanal. I løbet af larvestadiet bliver larven hyppigt fodret af ammebier. I de første par dage får de udelukkende den næringsrige fodersaft, som ammebierne producerer ved hjælp af fodersaftkirtlerne. Senere bliver fodersaften mindre nærings- og vitaminrig og bliver suppleret med nektar og pollen. For at kunne vokse og øge sin vægt, skifter larven sin hud fem gange i løbet af larvestadiet. Herefter går larven i gang med at spinde sig selv ind i silke, som den producerer ved hjælp af spytkirtlerne. Når den har spundet sig helt ind, ligger den ubevægelig i et døgns tid, hvorefter den går i gang med sit sidste hudskifte og bliver til en puppe. Under forvandlingen fra larve til puppe sker der en nedbrydning og opbygning af organerne.</p>",
    image: "js/lib/images/beeLarva.png"
};
var conPupa = {
    name: "conPupa",
    text: "Puppe",
    content: "<h1>Puppe</h1><img src='js/lib/images/beePupa.jpg' class='textBoxImage'><p class='header'>Facts om biens puppe:</p><p>Under opbygningen kan biens ydre genkendes i puppen. Puppen er til at begynde med hvid, og den ligger helt stille i den forseglede celle.I takt med at tiden går, begynder først puppens øjne, dernæst resten af kroppen at tage farve. De sidste par dage ligger bien og færdigudvikles inden den gnaver sig ud af cellen for at begynde tilværelsen som voksen. Det kaldes, at bien kryber.</p>",
    image: "js/lib/images/beePupa.png"
};
var conAdult = {
    name: "conAdult",
    text: "Voksen",
    content: "<h1>Voksen bi</h1><img src='js/lib/images/beeAdult.jpg' class='textBoxImage'><p class='header'>Facts om voksne bier (kan glædeligt ændres):</p><p>Efter bien har været igennem alle tidligere stadier er den nu voksen. En honningbi kan dog ikke leve alene i særlig lang tid. Den kan ikke undvære de andre bier i bifamilien, og man siger derfor, at den er et socialt insekt. I en bifamilie er der mange tusinde bier. Der er flest om sommeren, hvor bierne har mest travlt. En bifamilie har mange forskellige opgaver og derfor skal der være mange bier til at klare opgaverne. Der er tre typer af bier i en bifamilie, nemlig arbejderbier, droner og dronning.</p>",
    image: "js/lib/images/beeAdult.png"
};

var containerArray = [conEgg, conLarva, conPupa, conAdult];
var paraNum = 0; //Current paragraph-number - determines what is current content
var content; //Currently selected content for the textbox

//Important values for positioning
var canvasWidth = 600; //The magic number controlling the scale of everything
var strokeWidth = canvasWidth / 125;
var fontSize = canvasWidth / 18; //Font-size for the text-box
var textSize = "" + fontSize + "px Verdana"; //Specific font-size for the life cycle
var center = canvasWidth / 2;
var radius = canvasWidth / 2.5;
var pointerWidth = canvasWidth / 60;

//Foreground-elements for the life cycle
var pointer;
var button1;
var button2;

//Color codes
var darkestColor = '#663300';
var darkColor = "#D99520";
var mediumColor = '#cc7722';
var lightestColor = '#FADA5E';
var buttonColor = "#FFFF00";
var highlightColor = "#FFFFFF";

//Initial creation of the textbox, sets the styling of it
function createTextBox() {
    content = containerArray[paraNum].content;
    $("#textBox").html(content)
        .css("color", darkestColor)
        .css("font-family", "Verdana")
        .css("font-size", fontSize / 1.5)
        .css("border", "solid " + darkestColor + " " + strokeWidth + "px")
        .css("border-radius", strokeWidth * 5)
        .css("background-color", lightestColor)
        .css("padding-left", canvasWidth / 125);
}

//Initialization of everything related to the canvas, including creation of the elements in it
function init() {
    stage = new createjs.Stage("canvas");
    createTextBox();
    start();
    createjs.Ticker.setFPS(60);
    createjs.Ticker.addEventListener("tick", onTick);
}

//Creation of all canvas-elements
function start() {
    centerContent();
    createLabels(); //Creation of labels furthest behind in the model
    createBackgroundCircle(); //Creation of background circle
    separateCircleContent(); //Creates lines for separation of content on the circle
    createRotatingElements(); //Creates rotating images and text
    createForegroundCircle(); //Creates foreground-circle to surround buttons
    renderButtons("both", buttonColor, darkestColor); //Initial creation of both buttons
    stage.addChild(conDynamic, conStatic); //Adds everything to the canvas


    function centerContent() {
        //Positioning of dynamic content-container
        conDynamic = new createjs.Container();
        conDynamic.x = center;
        conDynamic.y = center;

        //Positioning of static content-container
        conStatic = new createjs.Container();
        conStatic.x = conDynamic.x;
        conStatic.y = conDynamic.y;
    }

    function createLabels() {
        //Generation of the text-labels behind the model
        for (i = 0; i < containerArray.length; i++) { //Still works if the containerArray changes size

            //Creates circle to be cut into labels
            textLabelCircle = new createjs.Shape(
                new createjs.Graphics().beginFill(lightestColor).setStrokeStyle(strokeWidth).beginStroke(darkestColor).drawCircle(0, 0, 1.225 * radius));

            //Creates background for labels to give them an outline, despite the mask
            textLabelOutlineX = (0.958 * canvasWidth) / 2;
            textLabelOutlineY = (1.02 * canvasWidth) / 5;
            textLabelOutline = new createjs.Shape(
                new createjs.Graphics().beginFill(darkestColor).setStrokeStyle(strokeWidth).beginStroke(darkestColor).drawRect(0, 0, textLabelOutlineX, textLabelOutlineY));
            textLabelOutline.regX = textLabelOutlineX;
            textLabelOutline.regY = textLabelOutlineY / 2;
            textLabelOutline.rotation = i * (360 / containerArray.length);

            //Creates mask for the circle, so we get nice label-shapes rather than a whole circle
            textLabelMask = new createjs.Shape(new createjs.Graphics().beginFill(darkestColor).drawRect(0, 0, canvasWidth, canvasWidth / 5));
            textLabelMask.regX = canvasWidth / 2;
            textLabelMask.regY = canvasWidth / 10;
            textLabelMask.rotation = i * (360 / containerArray.length);
            textLabelCircle.mask = textLabelMask;

            conDynamic.addChild(textLabelOutline, textLabelCircle);
        }
    }

    function createBackgroundCircle() {
        //Creates the main circle of the model, enveloping images and inner circle
        bgCircle = new createjs.Shape(new createjs.Graphics().beginFill(darkColor).setStrokeStyle(strokeWidth).beginStroke(darkestColor).drawCircle(0, 0, radius));
        conDynamic.addChild(bgCircle);
    }

    function separateCircleContent() {
        //Divides the main circle with lines
        for (i = 0; i < (containerArray.length); i++) { //still works if the containerArray changes size
            bgDivision = new createjs.Shape(new createjs.Graphics().beginFill(darkestColor).drawRect(0, 0, 2 * radius, canvasWidth / 125));
            bgDivision.regX = radius;
            bgDivision.regY = canvasWidth / 250;
            bgDivision.rotation = 45 + i * (360 / containerArray.length);
            conDynamic.addChild(bgDivision);
        }
    }

    function createRotatingElements() {
        //Creates images and text that rotates with the circle
        for (i = 0; i < containerArray.length; i++) {
            //We get our data from the container-array
            var obj = containerArray[i];
            var containerName = new createjs.Container();

            //Creates the image
            image = new createjs.Bitmap(obj.image);
            image.scaleX = image.scaleY = 0.4;
            image.regX = 250;
            image.regY = 400 * image.scaleX;
            image.y = -(canvasWidth / 3.5);

            //Creates the text element
            textElement = new createjs.Text(obj.text, textSize, darkestColor).setTransform(0, (-radius * 1.125), 1, 1);
            textElement.textBaseline = "middle";
            textElement.textAlign = "center";

            containerName.addChild(image, textElement);
            containerName.rotation = i * (360 / containerArray.length); //sets the rotation of each element. Still fits if we expand the containerArray
            conDynamic.addChild(containerName);
        }
    }

    function createForegroundCircle() {
        //Circles in the center of the model
        fgCircle = new createjs.Shape(
            new createjs.Graphics().beginFill(mediumColor).setStrokeStyle(strokeWidth).beginStroke(darkestColor).drawCircle(0, 0, radius / 4));
        fgCircle2 = new createjs.Shape(
            new createjs.Graphics().beginFill(darkestColor).setStrokeStyle(strokeWidth / 2).beginStroke(darkestColor).drawCircle(0, 0, radius / 16));
        //Rotating pointer 
        pointer = new createjs.Shape(new createjs.Graphics().beginFill(darkColor).drawRect(0, 0, pointerWidth, pointerWidth));
        pointer.regX = pointerWidth / 2;
        pointer.regY = pointerWidth / 2;
        pointer.rotation = 45;
        conDynamic.addChild(fgCircle, fgCircle2, pointer); //Technically everything rotates, but the circles look static
    }

    function renderButtons(buttons, color, strokeColor) {
        //Creates one or two buttons, depending on the parameters

        //Creates both buttons with the specified style
        if (buttons == "both") {
            button1 = new createjs.Shape(new createjs.Graphics().beginFill(color).setStrokeStyle(strokeWidth / 2).beginStroke(strokeColor).drawPolyStar(-30, 0, canvasWidth / 30, 3, 0, 180));
            button2 = new createjs.Shape(new createjs.Graphics().beginFill(color).setStrokeStyle(strokeWidth / 2).beginStroke(strokeColor).drawPolyStar(30, 0, canvasWidth / 30, 3, 0, 0));
            conStatic.addChild(button1, button2);
            //Only creates button number 1 with the specified style
        } else if (buttons == "1") {
            button1 = new createjs.Shape(new createjs.Graphics().beginFill(color).setStrokeStyle(strokeWidth / 2).beginStroke(strokeColor).drawPolyStar(-canvasWidth / 20, 0, canvasWidth / 30, canvasWidth / 200, 0, 180));
            conStatic.addChild(button1);
            //Only creates button number 1 with the specified style
        } else if (buttons == "2") {
            button2 = new createjs.Shape(new createjs.Graphics().beginFill(color).setStrokeStyle(strokeWidth / 2).beginStroke(strokeColor).drawPolyStar(canvasWidth / 20, 0, canvasWidth / 30, canvasWidth / 200, 0, 0));
            conStatic.addChild(button2);
        }

    }

    var turnable = true; //Determines whether or not the model can be interacted with. True by default.

    //Button 1 click event
    button1.addEventListener('click', function (e) {
        if (turnable) {
            turnable = false; //Turns off interactivity
            renderButtons("1", highlightColor, buttonColor); //Redraws the button with the active-style
            rotateRight(); //Rotates model
            setParagraphMinus(); //Changes active textbox-content
            updateTextBox();

        } else {
            return;
        }
        setTimeout(function () {
            turnable = true; //Turns interactivity back on
            renderButtons("1", buttonColor, darkestColor); //Redraws the button with the inactive-style
        }, 1000);
    });

    //Button 2 click event (essentially the same as button 1, except the other way)
    button2.addEventListener('click', function (e) {
        if (turnable) {
            turnable = false;
            renderButtons("2", highlightColor, buttonColor);
            rotateLeft();
            setParagraphPlus();
            updateTextBox();

        } else {
            return;
        }
        setTimeout(function () {
            turnable = true;
            renderButtons("2", buttonColor, darkestColor);
        }, 1000);
    });

    function rotateLeft() {
        //Rotates model left
        createjs.Tween.get(conDynamic).to({
            rotation: conDynamic.rotation - (360 / containerArray.length) //Still works if containerArray changes length
        }, 1000);
    }

    function rotateRight() {
        //Rotates model right
        createjs.Tween.get(conDynamic).to({
            rotation: conDynamic.rotation + (360 / containerArray.length) //Still works if containerArray changes length
        }, 1000);
    }

    function updateTextBox() {
        //Changes the content of the textbox
        content = containerArray[paraNum].content;
        $("#textBox").html(content);
    }

    function setParagraphPlus() {
        //Cycles the possible numbers within the containerArray
        paraNum = ((paraNum + 1) % containerArray.length);
    }

    function setParagraphMinus() {
        //Cycles the possible numbers within the containerArray (and avoids negative numbers)
        if (paraNum == 0) {
            paraNum = ((containerArray.length - 1) % containerArray.length);
        } else {
            paraNum = ((paraNum - 1) % containerArray.length);
        }
    }
}

function onTick() {

    stage.update(); //Updates the canvas repeatedly to ensure that animation is shown

}
