function showFrontpage() {
    $("#iframe").attr("src", "/frontpage");
}

function showQuiz() {
    $("#iframe").attr("src", "/quiz");
}
var anatomyBool = localStorage.setItem("anatomyBool", "true");

function showAnatomy() {
    var anatomyBool = localStorage.getItem("anatomyBool");
    if (anatomyBool == "true") {
        findBeeBool = localStorage.setItem("findBeeBool", "true");
        //tilføj class til næste knap
        $("#TTDbt").attr("class", "buttonGroup1"); //CHECK DENNE
        $("#iframe").attr("src", "/anatomy");
    }

}

function showTTD() {
    var findBeeBool = localStorage.getItem("findBeeBool");
    if (findBeeBool == "true") {
        beehiveBool = localStorage.setItem("beehiveBool", "true");
        //tilføj class til næste knap
        $("#beehivebt").attr("class", "buttonGroup1");
        $("#iframe").attr("src", "/tellthedifference");
    }
}

function showBeehive() {
    var beehiveBool = localStorage.getItem("beehiveBool");
    if (beehiveBool == "true") {
        lifecycleBool = localStorage.setItem("lifecycleBool", "true");
        //tilføj class til næste knap
        $("#lifecyclebt").attr("class", "buttonGroup1");
        $("#iframe").attr("src", "/beehive");
    }
}

function showLifecycle() {
    var lifecycleBool = localStorage.getItem("lifecycleBool");
    if (lifecycleBool == "true") {
        $("#iframe").attr("src", "/lifecycle");
    }
}

var article1bool = localStorage.setItem("article1bool", "false");
var article2bool = localStorage.setItem("article2bool", "false");
var article3bool = localStorage.setItem("article3bool", "false");
var article4bool = localStorage.setItem("article4bool", "false");
var article5bool = localStorage.setItem("article5bool", "false");
var article6bool = localStorage.setItem("article6bool", "false");

function setBoolFalse() {
    article1bool = localStorage.setItem("article1bool", "false");
    article2bool = localStorage.setItem("article2bool", "false");
    article3bool = localStorage.setItem("article3bool", "false");
    article4bool = localStorage.setItem("article4bool", "false");
    article5bool = localStorage.setItem("article5bool", "false");
    article6bool = localStorage.setItem("article6bool", "false");
};

function showArticle(article) {
    setBoolFalse();
    localStorage.setItem(article, "true");
    $("#iframe").attr("src", "/articles");
}

$(document).ready(function () {


    $("#DUOBbt").hover(
        function () {
            $("#DUOBbt .dropdown-content").css("visibility", "visible").hide();
            $("#DUOBbt .dropdown-content").fadeIn(300);
        },
        function () {
            $("#DUOBbt .dropdown-content").fadeOut(300);
        }
    ); // end hover function

    $("#articlesbt").hover(
        function () {
            $("#articlesbt .dropdown-content").css("visibility", "visible").hide();
            $("#articlesbt .dropdown-content").fadeIn(300);
        },
        function () {
            $("#articlesbt .dropdown-content").fadeOut(300);
        }
    ); // end hover function
    var anatomyBool = localStorage.getItem("anatomyBool");
    var findBeeBool = localStorage.getItem("findBeeBool");
    var beehiveBool = localStorage.getItem("beehiveBool");
    console.log(beehiveBool);
    var lifecycleBool = localStorage.getItem("lifecycleBool");
    if (anatomyBool == "false" || anatomyBool == null) {
        $("#anatomybt").attr("class", "buttonGroupInactive");
    }
    if (findBeeBool == "false" || findBeeBool == null) {
        $("#TTDbt").attr("class", "buttonGroupInactive");
    }
    if (beehiveBool == "false" || beehiveBool == null) {
        $("#beehivebt").attr("class", "buttonGroupInactive");
    }
    if (lifecycleBool == "false" || lifecycleBool == null) {
        $("#lifecyclebt").attr("class", "buttonGroupInactive");
    }
}); //end ready function
