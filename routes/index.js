var express = require('express');
var router = express.Router();

/* GET mainpage. */
router.get('/', function (req, res, next) {
    res.render('mainpage', {
        title: 'mainpage'
    });
});

/* GET frontpage */
router.get('/frontpage', function (req, res, next) {
    res.render('frontpage', {
        title: 'frontpage'
    });
});

/* GET quiz */
router.get('/quiz', function (req, res, next) {
    res.render('DUOB/quiz', {
        title: 'quiz'
    });
});

/* GET biensAnatomi - directpage. */
router.get('/anatomy', function (req, res, next) {
    res.render('DUOB/anatomy', {
        title: 'anatomy'
    });
});

/* GET tellTheDifference - directpage. */
router.get('/tellthedifference', function (req, res, next) {
    res.render('DUOB/tellTheDifference', {
        title: 'tellTheDifference'
    });
});

/* GET beehive */
router.get('/beehive', function (req, res, next) {
    res.render('DUOB/beehive', {
        title: 'beehive'
    });
});

/* GET livsCyklus - directpage. */
router.get('/lifecycle', function (req, res, next) {
    res.render('DUOB/lifecycle', {
        title: 'Biens Livscyklus'
    });
});

/* GET articles */
router.get('/articles', function (req, res, next) {
    res.render('articles', {
        title: 'articles'
    });
});
/* GET teachers quiz */
router.get('/bquiz', function (req, res, next) {
    res.render('billionaire', {
        title: 'lærerquiz'
    });
});
/* AJAX */
/* GET article1 */
router.route('/getArticle')
    .post(function (req, res) {
        var json = req.json;
        var wantedArticle = req.body.wantedArticle;
        console.log("wantedArticle: " + wantedArticle);
        var data = json[wantedArticle];
        res.send(data);
    });


module.exports = router;
